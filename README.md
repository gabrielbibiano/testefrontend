# Teste de frontend

Este desafio foi criado para avaliar os candidatos as vagas de frontend

## O desafio

Criar uma aplicação, responsiva contendo as seguintes telas

![Menu](./layout/Menu.png)

![Lista de Produtos](./layout/Produtos.png)

![Produto](./layout/Produto.png)

## A aplicação deve

- Utilizar os mocks disponibilizados no Mocky IO(ou relativo) para popular as telas do menu, lista de produtos e detalhe de produto.
- Considerar as flags e atributos disponibilizados no response de cada mock a fim de determinar exibição do depatarmento, ordenação, variação de preços e avaliações.
- Ao clicar em COMPRAR na página de detalhes do produto os dados do produto em questão devem ser armazenados no `local storage` a fim de simular um carrinho de compras.

## Sobre o layout

A estrutura apresentada como modelo deve ser considerada apenas como referÊncia. Não é obrigatório seguir a mesma minuciosamente, mas vale considerar a sua estrutura. É de responsabilidade do candidato adaptar o modelo à versão desktop sem layout.

- O menu principal do cabeçalho é apenas para efeito de layout, não há necessidade de desenvolver outras páginas fora lista de produtos e detalhe do produto.
- O layout deve se adaptar e mudar de acordo com o tamanho da tela
- Testaremos em smartphones, tablets (modos portrait e landscape) e monitores a partir de 1024px atÃ© 1900px
- A largura máxima do conteúdo é 1100px

## O que será avaliado

- Componentização
- Gerenciamento de estado
- HTML semãntico, limpo e claro
- CSS responsivo, semântico, reutilizável e seguindo boas práticas
- Conhecimento de Javascript orientado a objeto, funcional e/ou reativo
- Utilização do git com git flow
- Testes unitários
- O código deverá funcionar no Chrome
- A experiência do usuário deve ser a melhor possível

## Tecnologias que você pode utilizar

Usar React, Vue ou em último caso, Angular 2+.

Fica à critério usar TypeScript ou não.

Usar gerenciadores de estado como Redux ou Vuex.

Na parte de CSS, você é livre para utilizar qualquer preprocessador, mas não é permitido utilizar nenhum framework CSS.

Usar webpack ou gulp.

## Diferenciais

- Clean code
- CSS BEM
- Código bem documentado
- TDD
- Linting

## Mocky IO

- **Departamentos**: http://www.mocky.io/v2/5cabacb430000057001031e1

- **Produtos**: http://www.mocky.io/v2/5cabafdc30000068001031f6

- **Produto**: http://www.mocky.io/v2/5cabb00c30000072001031f7

## Estrutura de diretórios

- **images**: Imagens dos produtos
- **data**: Arquivos JSON contendo os dados disponíveis no Mocky IO
- **layout**: Imagens da estrutura proposta para o desafio
